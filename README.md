# php管理sqlite数据库工具 phpliteadmin 汉化版

#### 介绍
汉化并简化phpliteadmin
sqlite是一个零配置的、事务性的 SQL 数据库引擎。
只有一个文件，php内置支持sqlite，不需要mysql那么多的配置，这在存储小型数据方面非常有优势。

#### 软件架构
phpliteadmin是一个基于php管理sqlite的系统。

 

#### 使用说明

1.  默认密码是admin123
 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 界面截图

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0822/164429_17618c8a_7684763.png "QQ截图20210822164358.png")
